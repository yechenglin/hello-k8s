FROM ubuntu:16.04
MAINTAINER YeChenglin <yechnlin@gmail.com>

# install demo
COPY hello-k8s /usr/bin

# run demo
CMD ["/usr/bin/hello-k8s"]
