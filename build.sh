#!/bin/bash

OWNER=yechenglin
REPO=hello-k8s
VERSION="v1.0.0"
ARCH="$1"

sudo docker build -t="${OWNER}/${REPO}:${VERSION}-${ARCH}" .
